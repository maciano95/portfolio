var aboutMe = document.getElementById("aboutMe");
var contact = document.getElementById('footer');


var myScrollFunc = () => {
  var y = window.scrollY;
  if (y >= 300) {
    aboutMe.style.display = "flex";
    aboutMe.className = "about__wrapper show-animate";
  } else {
    aboutMe.className = "about__wrapper hide";
  }
  
  if( y >= 1800) {
    contact.style.display = "flex";
  } else {
    contact.style.display = "none";
  }
};

window.addEventListener("scroll", myScrollFunc);


function topFunction() {
  document.documentElement.scrollTop = 0;
}

var date = new Date();
var footerDate = document.getElementById('footer-date').innerHTML = `Maciej ${date.getFullYear()}`;